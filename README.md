# jsjArchi








## Description
A bunch of scripts to use with jArchi plugin for Archi (https://www.archimatetool.com/)

Those scripts are provided 'as-is' or as examples of what can be achieved with this tool. Using them without adaptation may damage your model.

### Loader
The ```lib/Loader.js``` alleviates the burden of loading js files from other js files. Using the load function may throw exception when loading the same files more than once, if loaded file defines some variables.
I have encoutered this problem when writing my unit testing code, so I have created a Loader class to
- load a file only if it is not already loaded. The loader implements the singleton pattern and stores the canonical path all files loaded to discriminate if it should load a new file. 
- offer a findAndLoadFile that browse the \_\_SCRIPTS_DIR__ directory for a given filename
- offer a config service using a config.properties file at the root jsjArchi directory. Property is defined on a single line property.name=property.value. Comments line in this file begins with #.

The loader is initialized through ```lib\ScriptUtils.initScript function```
Then you can use it with 
```new Loader().findAndLoadFile("YourFileName.js");```

Getting the value of a property defined in config.properties is easy
```new Loader().getProperty("your_property_name");```

### Unit Testing
[see this page](docs/UnitTest.md)


## Installation
Just copy them in the scripts folder of your archi installation.




## Support
No support provided.

## Roadmap/Todos
- add a cache to Loader.findAndLoadFile function to improve performance




## Contributing

## Authors and acknowledgment
Thanks to Phil Beauvoir for this great tool !

## License


## Project status
WIP 

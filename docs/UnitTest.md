
# unit testing with jArchi scripts
- First write your script so that it can be easily tested. For instance, make the .ajs files only handles input/ouput and delegate the "real" business logic to another file/class
- write a file named --------Test.ajs in the "tests" folder. Write it so that it can be called alone or after/before other tests, make it idempotent. Use the various "assert" functions in TestUtils.js to validate the results.
- Launch the AllTests.ajs scripts. 

Obviously, you should always test on a model that is representative of your "real-world" model. One way to do this is to "save as" one of your model as a new "unittest.archimate", removes any sensitive data, and from now on, always run your test on this dedicated model.


## The script to test
Here a very basic script to count objects in the model and output the result in the jArchi console.

```javascript
// load the main js file
load(__DIR__ + "/./../lib/Utils.js");
//initialize everything useful : load other js files, create the loader
initScript();
//now that the loader is initialized, use it to load the service associated with this script 
//note that findAndLoad only needs the filename, and not the full path starting with __DIR__

new Loader().findAndLoadFile("ObjectsCountService.js");
// no user input to handle

// instantiate the service and call it
var counts = new ObjectsCountService().process("*");

// handles outputs 
logToConsoleAsCsv(";", "Type", "Qty");
//for each type output its count 
counts.forEach(function (value, key) {
    logToConsoleAsCsv(";", key, value);
});

```
# The business Logic / service

Here, the logic is a class function but it is not mandatory.

```javascript
class ObjectsCountService  {
    process(selection) {
        var counts = new Map()
        //get all items and count them by type
        $(selection).each(function (o) {
            //getRootTypeForType is a function defined in ModelUtils.js
            var rootType = getRootTypeForType(o.type)
            if (counts.has(rootType)) {
                counts.set(rootType, counts.get(rootType) + 1)
    
            } else {
                counts.set(rootType, 1)
            }
        })
    
    
        return counts
    }
    
}

```


# The ___Test.ajs file
The filename must ends with "Test" and the extension must be "ajs".

The file must include a test class that inherits from ArchiUnitTest.
the file should be initialized with 2 lines
```javascript
load(__DIR__ + "/./../../lib/Utils.js");
```
then call initTestScript with the name of the file you are going to test (this file must be under __SCRIPTS_DIR__ directory)
```javascript
initTestScript("ObjectsCountService.js");
```

ArchiUnitTest defines 3 functions : 
- prestest() : to initialize everything that is needed for your test (example : create folder and views)
- processtest() : call the business logic, validate the results with assert methods
- posttest() : remove/delete everything that was created in prestest.
- 
```javascript
/**
 * Initialize script (common to all test scripts)
 */
load(__DIR__ + "/./../../lib/Utils.js");
initTestScript("ObjectsCountService.js");




/**
 * 
 */
class YourTest extends ArchiUnitTest {
    constructor(param1, param2) {
        super();
        //do something
        
    }
 
    pretest() {
        super.pretest();
        //do something   
    }
    /**
     * invoke the script to test
     */
    processtest() {
       var viewService = new ViewService();
       viewService.addTitleBlock(this.testView);
       var note = viewService.getExistingNote(this.testView);
       assertNotNull("Title block added in view",note);
       if (note !==null) {
        assertNotNull("Title block has an author", note.prop("note_user"))
        assertNotNull("Title block has a date", note.prop("note_date"))
        assertNotNull("Title block has a documentation", note.labelExpression)
       } else {
        logTestResult("cannot test title block author ",  false);
        logTestResult("cannot test title block date ",  false);
        logTestResult("cannot test title block documentation ",  false);
       }
       
    }
    /**
     * delete the temporary test folder
     * 
     */
    posttest() {
        super.posttest();
        //do something  
    }
    
  }
```
finally invoke your test object
```
  new YourTest().doTest();
```




function initScript() {
    var loader = null;
    if (typeof Loader === "undefined") {
        //it the first time we call initScript so clear the console
        console.clear();
        load(__DIR__ + "/../lib/Loader.js")
    }
    loader = new Loader();
    loader.loadFile(__DIR__ + "/Constants.js");
    loader.loadFile(__DIR__ + "/ElementUtils.js");
    loader.loadFile(__DIR__ + "/LogUtils.js");
    loader.loadFile(__DIR__ + "/ModelUtils.js");
    loader.loadFile(__DIR__ + "/RelationUtils.js");
    loader.loadFile(__DIR__ + "/StringUtils.js");

}
function initTestScript(fileToTest) {
    initScript();
    new Loader().loadFile(__DIR__ + "/TestUtils.js");
    if (fileToTest !== undefined && fileToTest !== null) {
        new Loader().findAndLoadFile(fileToTest);
    }
}

/**
 * affiche une fenêtre de confirmation avant de lancer le traitement, si l'utilisateur ne confirme pas, le script s'arrête
 * @param {*} action : un texte libre qui décrit ce que fait le traitement
 * @param {*} scope  : le type d'objet sélectionné qui va être l'objet du traitement
 * @param {*} selection : la sélection 
 * @returns nothing
 */

function confirmWindow(action, scope, selection) {
    let message1 = "This script is going to execute following action \r\n    " + action + "\r\n on every " + scope + " selected"
    let message2 = "";
    
    let size =0;
    if (scope!=="*") {
        size = selection.filter(scope).size();
        
    } else {
        size = selection.size();
    }
    let message3="Your selection has  " + size; + "    " + scope + "(s)"
    if (size === 0) {
        message3 = "ERR, no " + scope + " selected";
    }

    let message4 = "\r\n Continue ?"

    let continueScript = window.confirm(message1 + "\r\n" + message2 + "\r\n" + message3 + "\r\n" + message4);

    if (continueScript) {
        return;
    } else {
        exit();
    }
}
/**
 * From current selection ,get all objets of type "scope".  If none found try to get them in children of select objets
 * If none found, try to get from parent of select object.
 * Finally, if none found and allowWholeModel = true get all objets of this type from model
 * 
 * @param {*} selection 
 * @param {*} scope 
 * @param {*} allowWholeModel true si on permet de sélectionner tous les objets de type scope du modèle
 * @returns une selection
 */
function prepareSelection(selection, scope, allowWholeModel) {
    
    let newSelection = selection;
    if (scope !=="*") {
        newSelection = newSelection.filter(scope);
    }
    
    
    if (newSelection.size() == 0) {
        //no object found, get all descendants
        newSelection = selection.find(scope)
        if (newSelection.size() == 0) {
            //no found get parent ? 
            newSelection = selection.parent(scope);
            if (newSelection.size() == 0) {

                if (allowWholeModel && window.confirm("No" + scope + " found in collection or children, do you want to select all objects of this type in current model ? ")) {
                    newSelection = $(scope)
                } else {
                    log("no " + scope + " found in selection", LOG_LEVEL_ERROR)
                }
            }

        }
    }
    return newSelection

}
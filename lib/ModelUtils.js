
var typeCategoryMap = new Map();

/**
 * returns the category associated with the given type
 * ex : Strategy for ressource, Technology for node
 * see https://github.com/archimatetool/archi-scripting-plugin/wiki/jArchi-Collection
 * Folder has no category so it is tagged as Folder.
 * @param {*} type 
 */



function getCategoryForType(type) {
    if (typeCategoryMap.size == 0) {
        typeCategoryMap.set('resource', 'Strategy'); typeCategoryMap.set('capability', 'Strategy'); typeCategoryMap.set('course-of-action', 'Strategy'); typeCategoryMap.set('value-stream', 'Strategy');
        typeCategoryMap.set('business-actor', 'Business'); typeCategoryMap.set('business-role', 'Business'); typeCategoryMap.set('business-collaboration', 'Business'); typeCategoryMap.set('business-interface', 'Business'); typeCategoryMap.set('business-process', 'Business'); typeCategoryMap.set('business-function', 'Business'); typeCategoryMap.set('business-interaction', 'Business'); typeCategoryMap.set('business-event', 'Business'); typeCategoryMap.set('business-service', 'Business'); typeCategoryMap.set('business-object', 'Business'); typeCategoryMap.set('contract', 'Business'); typeCategoryMap.set('representation', 'Business'); typeCategoryMap.set('product', 'Business');
        typeCategoryMap.set('application-component', 'Application'); typeCategoryMap.set('application-collaboration', 'Application'); typeCategoryMap.set('application-interface', 'Application'); typeCategoryMap.set('application-function', 'Application'); typeCategoryMap.set('application-process', 'Application'); typeCategoryMap.set('application-interaction', 'Application'); typeCategoryMap.set('application-event', 'Application'); typeCategoryMap.set('application-service', 'Application'); typeCategoryMap.set('data-object', 'Application');
        typeCategoryMap.set('node', 'Technology'); typeCategoryMap.set('device', 'Technology'); typeCategoryMap.set('system-software', 'Technology'); typeCategoryMap.set('technology-collaboration', 'Technology'); typeCategoryMap.set('technology-interface', 'Technology'); typeCategoryMap.set('path', 'Technology'); typeCategoryMap.set('communication-network', 'Technology'); typeCategoryMap.set('technology-function', 'Technology'); typeCategoryMap.set('technology-process', 'Technology'); typeCategoryMap.set('technology-interaction', 'Technology'); typeCategoryMap.set('technology-event', 'Technology'); typeCategoryMap.set('technology-service', 'Technology'); typeCategoryMap.set('artifact', 'Technology');
        typeCategoryMap.set('equipment', 'Physical'); typeCategoryMap.set('facility', 'Physical'); typeCategoryMap.set('distribution-network', 'Physical'); typeCategoryMap.set('material', 'Physical');
        typeCategoryMap.set('stakeholder', 'Motivation'); typeCategoryMap.set('driver', 'Motivation'); typeCategoryMap.set('assessment', 'Motivation'); typeCategoryMap.set('goal', 'Motivation'); typeCategoryMap.set('outcome', 'Motivation'); typeCategoryMap.set('principle', 'Motivation'); typeCategoryMap.set('requirement', 'Motivation'); typeCategoryMap.set('constraint', 'Motivation'); typeCategoryMap.set('meaning', 'Motivation'); typeCategoryMap.set('value', 'Motivation');
        typeCategoryMap.set('work-package', 'Implementation & Migration'); typeCategoryMap.set('deliverable', 'Implementation & Migration'); typeCategoryMap.set('implementation-even', 'Implementation & Migration'); typeCategoryMap.set('plateau', 'Implementation & Migration'); typeCategoryMap.set('gap', 'Implementation & Migration');
        typeCategoryMap.set('location', 'Other'); typeCategoryMap.set('grouping', 'Other'); typeCategoryMap.set('junction', 'Other');
        typeCategoryMap.set('composition-relationship', 'Relationships'); typeCategoryMap.set('aggregation-relationship', 'Relationships'); typeCategoryMap.set('assignment-relationship', 'Relationships'); typeCategoryMap.set('realization-relationship', 'Relationships'); typeCategoryMap.set('serving-relationship', 'Relationships'); typeCategoryMap.set('access-relationship', 'Relationships'); typeCategoryMap.set('influence-relationship', 'Relationships'); typeCategoryMap.set('triggering-relationship', 'Relationships'); typeCategoryMap.set('flow-relationship', 'Relationships'); typeCategoryMap.set('specialization-relationship', 'Relationships'); typeCategoryMap.set('association-relationship', 'Relationships');
        typeCategoryMap.set('diagram-model-note', 'Other Visual Objects'); typeCategoryMap.set('diagram-model-group', 'Other Visual Objects'); typeCategoryMap.set('diagram-model-connection', 'Other Visual Objects'); typeCategoryMap.set('diagram-model-image', 'Other Visual Objects'); typeCategoryMap.set('diagram-model-reference', 'Other Visual Objects'); typeCategoryMap.set('sketch-model-sticky', 'Other Visual Objects'); typeCategoryMap.set('sketch-model-actor', 'Other Visual Objects'); typeCategoryMap.set('canvas-model-block', 'Other Visual Objects'); typeCategoryMap.set('canvas-model-sticky', 'Other Visual Objects'); typeCategoryMap.set('canvas-model-image', 'Other Visual Objects');
        typeCategoryMap.set('archimate-diagram-model', 'Views'); typeCategoryMap.set('sketch-model', 'Views'); typeCategoryMap.set('canvas-model', 'Views');
        typeCategoryMap.set('folder', 'Folder')
    }
    return typeCategoryMap.get(type);
}

/**
 * returns the root type of the given object (Concept, Relationships, Views, Other)
 * @param {*} type 
 */
function getRootTypeForType(type) {
    res = getCategoryForType(type);
    if (res ==="Strategy" || res ==="Business"|| res ==="Application"|| res ==="Technology"|| res ==="Physical"|| res ==="Motivation" || res ==="Implementation & Migration") {
        res = "Concept"
    } else if (res==="Other" ||res==="Other Visual Objects" ||res==="Folder" ) {
        res = "Other"

    }
    return res;
}



/**
 * Various methods to write to jArchi console
 * 
 * 
 * 
 */


/**
 * Display a message in jArchi console
 * 
 * @param {*} tolog  : the string to log
 * @param {*} level  : level LOG_LEVEL_INFO, LOG_LEVEL_DEBUG, LOG_LEVEL_ERROR, LOG_LEVEL_WARNING
 */
function log(tolog, level = LOG_LEVEL_INFO) {
    if (debug) {
        if (level === LOG_LEVEL_DEBUG) {
            console.log(LOG_LEVEL_DEBUG + ">>>>>>" + tolog);
        }
    }



    if (level === LOG_LEVEL_ERROR) {
        console.log(LOG_LEVEL_ERROR + "!!!!!!!!!" + tolog);


    }
    if (level === LOG_LEVEL_WARNING) {
        console.log(LOG_LEVEL_WARNING + "!!!" + tolog);


    }
    if (level === LOG_LEVEL_INFO) {
        console.log(LOG_LEVEL_INFO + ":" + tolog);


    }
}

function writeToConsole(toWrite) {
    toWrite = toWrite.replace(/(?:\r\n|\r|\n)/g, ' ');
    console.log(toWrite);
}

function logToConsoleAsCsv(separator = ";") {

    const args = Array.prototype.slice.call(arguments, 1);
    writeToConsole(args.join(separator));
}
function outputResultSetToConsole(resultSet, format = CSV_OUTPUT_FORMAT, printHeaders = true) {
    if (format === CSV_OUTPUT_FORMAT) {
        if (printHeaders) {
            logToConsoleAsCsv(";", ...resultSet.headers)
        }
        for (i = 0; i < resultSet.results.length; i++) {
            logToConsoleAsCsv(";", ...resultSet.results[i])
        }

    }

}

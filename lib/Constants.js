const LOG_LEVEL_WARNING = "WARNING"
const LOG_LEVEL_DEBUG = "DEBUG"
const LOG_LEVEL_INFO = "INFO"
const LOG_LEVEL_ERROR = "ERROR"



/**
 * Constants for views
 * 
 */

//properties
VIEW_TYPE_PROP = '_view_type'
VIEW_TYPE_PROP_FUNC = 'C08'
VIEW_TYPE_PROP_APP= 'C09'
VIEW_TYPE_PROP_TEC = 'C10'
VIEW_RELATE_PROP = '_relate_to'
const VIEW_DEFAULT_VIEWS_NAMES = ["-1-Architecture fonctionnelle","-2-Architecture applicative", "-3-Architecture technique"]
const VIEW_DEFAULT_VIEWS_TYPES = [VIEW_TYPE_PROP_FUNC, VIEW_TYPE_PROP_APP, VIEW_TYPE_PROP_TEC]


/**
 * Properties for flow_relationships
 */
const FLOW_PROP_LABEL ="_label"
const FLOW_PROP_TYPE ="_flow_type"
const FLOW_PROP_PROTOCOL ="_protocol"
const FLOW_PROP_PORT ="_port"
const FLOW_PROP_CRITICAL ="_critical"

const FLOW_NAME_REGEXP_INDEX = "[Ff].*?[.:,]"
const FLOW_NAME_REGEXP_LABEL = "[.:,].*\\("
const FLOW_NAME_REGEXP_PROTOCOL = "\\(.*\\)"


const FLOW_TYPE_VALUE = "Point à point";
const FLOW_IP_OR_FQDN_VALUE = "FQDN";
const FLOW_SIZE_VALUE = "100ko";
const FLOW_FREQUENCY_VALUE = "100/j";
const FLOW_PLANNING_VALUE = "24/7";
const FLOW_SEPARATOR = "\t";

// default ports for protocols
var FLOW_DEFAULT_PORTS = new Map()
FLOW_DEFAULT_PORTS.set('https', 443)
FLOW_DEFAULT_PORTS.set('http', 80)
FLOW_DEFAULT_PORTS.set('http/https', '80/443')
FLOW_DEFAULT_PORTS.set('http(s)', '80/443')
FLOW_DEFAULT_PORTS.set('ssh', 22)
FLOW_DEFAULT_PORTS.set('sftp', 22)
FLOW_DEFAULT_PORTS.set('ftp', 21)
FLOW_DEFAULT_PORTS.set('sql*net', 1526)
FLOW_DEFAULT_PORTS.set('sqlnet', 1526)
FLOW_DEFAULT_PORTS.set('sql net', 1526)
FLOW_DEFAULT_PORTS.set('ldaps', 636)

//
var _TEST_OK_COUNT=0
var _TEST_KO_COUNT=0


var CSV_OUTPUT_FORMAT="csv"

//archimate objects definition
const ARCHIMATE_VIEWS_TYPES=['archimate-diagram-model','sketch-model','canvas-model']
const ARCHIMATE_CONCEPTS_STRATEGY_TYPES=['resource','capability','course-of-action','value-stream']
const ARCHIMATE_CONCEPTS_BUSINESS_TYPES=['business-actor','business-role','business-collaboration','business-interface','business-process','business-function','business-interaction','business-event','business-service','business-object','contract','representation','product']
const ARCHIMATE_CONCEPTS_APPLICATION_TYPES=['application-component','application-collaboration','application-interface','application-function','application-process','application-interaction','application-event','application-service','data-object']
const ARCHIMATE_CONCEPTS_TECHNOLOGY_TYPES=['node','device','system-software','technology-collaboration','technology-interface','path','communication-network','technology-function','technology-process','technology-interaction','technology-event','technology-service','artifact']
const ARCHIMATE_CONCEPTS_PHYSICAL_TYPES=['equipment','facility','distribution-network','material']
const ARCHIMATE_CONCEPTS_MOTIVATION_TYPES=['stakeholder','driver','assessment','goal','outcome','principle','requirement','constraint','meaning','value']
const ARCHIMATE_CONCEPTS_IMPLEMENTATION_TYPES=['work-package','deliverable','implementation-even','plateau','gap']
const ARCHIMATE_OTHER_TYPES=['location','grouping','junction']
const ARCHIMATE_RELATIONSHIP_TYPES=['composition-relationship','aggregation-relationship','assignment-relationship','realization-relationship','serving-relationship','access-relationship','influence-relationship','triggering-relationship','flow-relationship','specialization-relationship','association-relationship',]
const ARCHIMATE_VISUAL_TYPES=['diagram-model-note','diagram-model-group','diagram-model-connection','diagram-model-image','diagram-model-reference','sketch-model-sticky','sketch-model-actor','canvas-model-block','canvas-model-sticky','canvas-model-image']
const ARCHIMATE_CONCEPTS_ALL_TYPES= [].concat(ARCHIMATE_CONCEPTS_STRATEGY_TYPES, ARCHIMATE_CONCEPTS_BUSINESS_TYPES,ARCHIMATE_CONCEPTS_APPLICATION_TYPES,ARCHIMATE_CONCEPTS_TECHNOLOGY_TYPES,ARCHIMATE_CONCEPTS_PHYSICAL_TYPES,ARCHIMATE_CONCEPTS_MOTIVATION_TYPES,ARCHIMATE_CONCEPTS_IMPLEMENTATION_TYPES)

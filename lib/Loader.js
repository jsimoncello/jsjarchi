/**
 * Utility class to load javascript module and avoid loading them if already loaded
 * this class can be used all alone
 * 
 * 
 */

class Loader {
    
    constructor() {
        if (Loader._instance) {
            return Loader._instance;
        }
        Loader._instance = this;
        this.properties=null;
        this.loadedFiles = [];
        //all files found are store in a map key filename, value canonicalpath
        this.loadableFiles = new Map();
        //this can be slow if your scripts dir has a lot of files, just change the path to limit its scope
        this.browseFolder(__SCRIPTS_DIR__);
        var CONFIG_FILE="config.properties"
        if (this.loadableFiles.get(CONFIG_FILE) !== undefined) {
            this.properties = new Map();
            //use java classes to read the content of the properties file
            //I have tried with java.util.Properties but have not found a workaround for "no applicable overload found" polyglot errors
            var ReaderClass= Java.type('java.io.FileReader');
            var reader = new ReaderClass(this.loadableFiles.get(CONFIG_FILE));
            var BufferedReaderClass=Java.type('java.io.BufferedReader');
            var bf = new BufferedReaderClass(reader);
            var line ="";
            while ((line = bf.readLine())!=null) {
                //ignore comments lines
                if (!line.startsWith("#")) {
                    var idx;
                    if ((idx = line.indexOf("="))>-1) {
                        //extract property name (left of equal sign) and value (right of equal sign)
                        var key = line.substring(0, idx)
                        var value=line.substring(idx+1);
                        //add this new property
                        this.properties.set(key, value);
                        

                    }
                }
            }
            
         
         
        }
    }
    /**
     * get a property from a config.properties file (if it exists)
     * @param {string} key 
     * @returns {string} the value of the property or null if no properties were found
     */
    getProperty(key) {
        if (this.properties!==null) {
            return this.properties.get(key);
        } else {
            return null;
        }

    }


    /**
     * load a javascript file if not already loaded
     * @param {*} filePath
     */
    loadFile(filePath) {
        var FileClass = Java.type('java.io.File');
        //workaround add empty string to force casting to string, otherwise Polyglott throws an exception as it cannot chose wich constructor to use
        var file = new FileClass(filePath + "");

        if (file.exists()) {
            //as file path can be complex absolute and relative folders use canonical path to discriminate if file has already been loaded
            //with a different path
            var correctedFilePath = file.getCanonicalPath();

            if (this.loadedFiles.includes(correctedFilePath)) {
                //console.log(correctedFilePath + "already loaded");
            } else {
                //console.log("loading " + correctedFilePath);
                load(correctedFilePath);
                this.loadedFiles.push(correctedFilePath)
            }
        } else {
            console.error(filePath + " not found")
        }
    }

    /**
     * Find and load a file with given name and extension in __SCRIPTS_DIR__
     * @param {*} fileName 
     */
    findAndLoadFile(fileName) {
        if (this.loadableFiles.get(fileName) !== undefined) {
            new Loader().loadFile(this.loadableFiles.get(fileName));
        } else {
            throw new Error("unable to load " + fileName);
        }
    }
    /**
     * recursive method called by findAndLoadFile
     * the path to search for js, ajs and config.properties
     *
     */
    browseFolder(path) {
        var FileClass = Java.type('java.io.File');
        var folder = new FileClass(path);

        var files = folder.listFiles();
        if (files !== null) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (file.isDirectory()) {
                    this.browseFolder(file.getCanonicalPath());
                } else {
                    if (file.getName().endsWith(".js") || file.getName().endsWith(".ajs")|| file.getName().endsWith("config.properties")) {
                        this.loadableFiles.set(file.getName(), file.getCanonicalPath());

                    }
                }
            }
        }
    }
}
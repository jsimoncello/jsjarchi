/**
 * class that holds results so that it makes printing to console or 
 * 
 * 
 * 
 */

class ResultSet {
    constructor(headers) {
        this.headers=headers
        this.results = []
    }
    addResult(values) {
        this.results.push(values)
    }
    size() {
        return this.results.length;
    }
    
}
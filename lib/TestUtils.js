/*
 * Generic file which includes all constant and functions  for test scripts
 *
 *
 * Requires: jArchi -
 * Author: Jérémy Simoncello
 */

/*################################################

@TODO : add an option to write output to a file instead of console


################################################
**/


function logTestResult(message, pass) {
    if (pass) {
        console.setTextColor(0, 150 , 0);
        _TEST_OK_COUNT+=1;
    } else {
        console.setTextColor(255, 0 , 0);
        _TEST_KO_COUNT+=1;
              
    }
    console.log(pass+";"+ message) ;
    console.setDefaultTextColor();

}
function assertArrayContains(testDesc,array, expectedValue) {
    var valueFound=false;
    for (i = 0 ; i < array.length; i++) {
        if (compareArrays(array[i],expectedValue)) {
            valueFound = true;
            break;
        }

    }
    if (valueFound) {
        logTestResult(testDesc +"<" + expectedValue + "> found ;" ,  true);
    } else {
        logTestResult(testDesc +";expecting <" + expectedValue + "> not found " , false);
    }

}
function assertArrayEquals(testDesc, arrayToTest, expectedArray) {
    if (compareArrays(arrayToTest, expectedArray)) {
        logTestResult(testDesc +"<" + expectedArray + "> found ;" ,  true);
    } else {
        logTestResult(testDesc +";expecting <" + expectedArray + "> not found : was " + arrayToTest , false);
    }



}
function assertEquals(testDesc,valueToTest, expectedValue) {
    if (valueToTest ===expectedValue) {
        logTestResult(testDesc +"<" + expectedValue + "> found ;" ,  true);
    } else {
        logTestResult(testDesc +";expecting <" + expectedValue + "> got <" + valueToTest +">", false);
    }

}
function assertOnlyOneExists(testDesc,valueToTest) {
    if ($(valueToTest).first() !== null) {
        logTestResult( testDesc +";" ,  true);
    } else {
        logTestResult( testDesc +" not found;" ,  false);
    }

}
function assertNotNull(testDesc,valueToTest) {
    if (valueToTest !== null) {
        logTestResult(testDesc +";" ,  true);
    } else {
        logTestResult(testDesc +" not found;" ,  false);
    }

}
function compareArrays(a, b) {
    if (a.length !== b.length) {
        
        return false;
    }
    else {
        
      // Comparing each element of your array
      for (var i = 0; i < a.length; i++) {
        
        if (a[i] !== b[i]) {
      
          return false;
        }
      }
      
      return true;
    }
  };
class ArchiUnitTest {
    /** main methods to call when launching a test */
    doTest() {
        this.pretest();
        this.processtest();
        this.posttest()
    }
    /**
     * if needed create temp objects for this test
     */
    pretest() {
        console.log ("pretest " + this.constructor.name);

        
        
    }
    processtest()  {
        console.log("executing test (do nothing)");
    }
    /**
     * if needed destroy objects created in pretest()
     */
    posttest() {
        //reset console color after tests
        console.setDefaultTextColor();
        console.log ("posttest " + this.constructor.name);
    }
}
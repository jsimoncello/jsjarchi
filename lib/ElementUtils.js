
/** 
 * get the content of the note associated to the given element
 * if more than one note is found, contents are concatenated.
 * @param el : 
*/
function getNote(el) {
    str = ""
    if (el != null) {
        $(el).children('diagram-model-note').each(function (note) {
            str = str + " "+ note.text;
        })
    }
    return str.trim();
}

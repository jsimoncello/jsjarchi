function padStart(targetLength,pad, str) {
    while(str.length<targetLength) {
        str = pad+str;
    }
    return str;

}
function isNumber(char) {
    if (typeof char !== 'string') {
      return false;
    }
  
    if (char.trim() === '') {
      return false;
    }
  
    return !isNaN(char);
  }

  function cleanString(str) {
    if (str==null) {
      str=""

    } else {
      str= str.trim()
      if (str.slice(-1)==',') {
        str= str.slice(0,-1)
      }
    }
    return str
  }

  function arrayToStringOfNames(str) {
    tmp=""
    for (var i=0; i< str.length;i++) {
      tmp = tmp + "," + str[i].name
    }
    return tmp

  }

  function arrayToString(str,sep) {
    var tmp=""
    for (var i=0; i< str.length;i++) {
      
      if (str[i]!=null && typeof str[i] !== 'undefined') {
        tmp = tmp + sep + str[i]
      }
    }
    if (tmp.length>0) {
      tmp = tmp.substring(1)
    }
    return tmp

  }    

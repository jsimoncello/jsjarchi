/**
 * @deprecated user ViewService.addTitleBlock
 * @param {*} archimateView 
 */
function addTitleBlock(archimateView) {

    var System = Java.type('java.lang.System');
    var note ;
    //test if a title block is already present in view
    $(archimateView).find().filter("diagram-model-note").each(function (n) {
        // if a note has a note_title then it is a diagram title block
        if (n.prop("note_title") !== null) {
            note = n;
            
        }

    });
    if (note == null) {
        note = archimateView.createObject("diagram-model-note", 10, 10, 300, 100, true);
    }
    
    note.prop("note_title",archimateView.name)
    note.setText(archimateView.documentation)
    note.prop("note_user",System.getProperty("user.name"))
    note.prop("note_date",new Date().toISOString().slice(0, 10))
    note.labelExpression = "${property:note_title}" +"\n "+ "${property:note_content}" +"\n "+ "${property:note_user}" +"\n "+ "${property:note_date}" +"\n "+ "${content}"
    note.fillColor = "#ffffff"


}
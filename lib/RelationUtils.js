
/**
 * return the default port associated with given protocol based on data in FLOW_DEFAULT_PORTS matrix (see ConstantUtils)
 * @param {*} protocol 
 * @returns a string
 */
function getDefaultPort(protocol) {
    //TODO : add lower/upper case support
    if (protocol !==null) {
        return FLOW_DEFAULT_PORTS.get(protocol.toLowerCase());
    } 
    return "";
    
      

}
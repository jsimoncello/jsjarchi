new Loader().findAndLoadFile("ResultSet.js")

class ObjectService {


    /**
     * returns a result set (elementId,conceptId,type,name) with all concepts that matchs the id given
     * 
     * @param {string} id 
     * @returns {ResultSet} 
     */
    getById(id) {
        var result = new ResultSet(["elementId", "conceptId", "type", "name"]);

        //search concepts
        $("*").each(function (o) {
            if (o.id.indexOf(id) > -1) {
                result.addResult(["", o.id, o.type, o.name]);
            }
        });
        //search visual objects
        $("view").find().each(function (o) {
            if (o.id.indexOf(id) > -1) {

                let conceptId = "";
                if (o.concept !== null && o.concept.id !== o.id) {
                    conceptId = o.concept.id;
                }
                result.addResult([o.id, conceptId, o.type, o.name]);


            }
        });
        return result;
    }
    /**
     * return, id, conceptid, type name of selected objects
     * @param {selection} selected 
     * @returns {ResultSet} 
     */
    getId(selected) {
        var result = new ResultSet(["elementId", "conceptId", "type", "name"]);
        var self = this;
        selected.each(function (o) {
            if (self.objectExists(o)) {
                var id = o.id;
                var conceptId = "";
                if (self.objectExists(o.concept)) {
                    conceptId = o.concept.id;
                    //if both ids are equals we have selected a concept do not display id twice
                    if (o.id === o.concept.id) {
                        id = "";
                    }


                }
                result.addResult([id, conceptId, o.type, o.name]);
            }

        });
        return result;
    }
    /**
     * 
     * @param {object} o 
     * @returns {boolean} true if o is different from null and undefined
     */
    objectExists(o) {
        return (o != null && o !== undefined);

    }
    /**
     * @public
     * @param {*} concept 
     * @param {*} targetType 
     */
    changeType(concept, targetType) {
        let relaxed = false;
        var self = this;
        $(concept).outRels().each(function (r) {

            if (!$.model.isAllowedRelationship(r.type, targetType, r.target.type)) {
                self.checkAndConvertRelationship(r, relaxed);
            }
        });

        $(concept).inRels().each(function (r) {

            if (!$.model.isAllowedRelationship(r.type, r.source.type, targetType)) {
                self.checkAndConvertRelationship(r, relaxed);
            }
        });

        concept.type = targetType;

    }

    checkAndConvertRelationship(r, relaxed = false) {

        if (relaxed) {
            r.documentation = 'This relationship has been converted from "' + r.type.replace(/-relationship$/, '') + '" to "association"\n' + r.documentation;
            r.type = "association-relationship";
        }
        else {
            window.alert('Relationship "' + r.name + '" from "' + r.source.name + '" to "' + r.target.name + '" will not be valid after conversion and "strict" mode is on. Conversion aborted.');
            exit();
        }
    }
}
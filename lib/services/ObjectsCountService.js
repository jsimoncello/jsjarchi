/**
 * 
 * 
 * 
 */
class ObjectsCountService  {
    process(selection) {
        var counts = new Map()
        //get all items and count them by type
        $(selection).each(function (o) {
            var rootType = getRootTypeForType(o.type)
            if (counts.has(rootType)) {
                counts.set(rootType, counts.get(rootType) + 1)
    
            } else {
                counts.set(rootType, 1)
            }
        })
    
    
        return counts
    }
    
}
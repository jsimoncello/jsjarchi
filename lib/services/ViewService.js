/**
 * service class for view object
 * 
 */

class ViewService {
    constructor() {

    }
    /**
     * @public
     * @param {*} folder 
     */
    createDefaultViews(folder) {
        var viewNames = VIEW_DEFAULT_VIEWS_NAMES;
        var viewTypes = VIEW_DEFAULT_VIEWS_TYPES;
        var currentObject = this;
        viewNames.forEach(function (viewName, i) {
            var view = model.createArchimateView(folder.name + viewName, folder)
            //we are in a nested function so "this" no longer reference current object 
            //there are multiple workarounds but I find this one the most readable.
            currentObject.addTitleBlock(view);
            view.prop(VIEW_TYPE_PROP, viewTypes[i])
            view.prop(VIEW_RELATE_PROP, folder.name)
        })


    }
    /**
     * @public
     * @param {*} archimateView 
     */
    addTitleBlock(archimateView) {
        var System = Java.type('java.lang.System');
        var note = this.getExistingNote(archimateView);
        //if no note was found in view create it
        if (note == null) {
            note = archimateView.createObject("diagram-model-note", 10, 10, 300, 100, true);
        }

        note.prop("note_title", archimateView.name)
        note.setText(archimateView.documentation)
        note.prop("note_user", System.getProperty("user.name"))
        note.prop("note_date", new Date().toISOString().slice(0, 10))
        note.labelExpression = "${property:note_title}" + "\n " + "${property:note_content}" + "\n " + "${property:note_user}" + "\n " + "${property:note_date}" + "\n " + "${content}"
        note.fillColor = "#ffffff"
    }
    /**
    * Return last diagram-model-note with a "note_title" property in view
    * @public
    */

    getExistingNote(archimateView) {
        var result = null;
        $(archimateView).find().filter("diagram-model-note").each(function (n) {
            // if a note has a note_title then it is a diagram title block

            if (n.prop("note_title") !== null) {
                result = n;

            }

        });
        return result;

    }
    /**
     * Sample checkView method.
     * @public
     * @param {archimate-diagram-model} archimateView 
     * @returns {ResultSet} with headers "viewName","level" and "message"
     */
    checkView(archimateView) {
        let result = new ResultSet(["level", "message", "viewName"]);
        this.checkElements(archimateView, 'application-component', ['NNA', 'Nom long', 'Statut'], result);
        this.checkSpecialization(archimateView, 'application-component', ['application', 'infrastructure', 'infrastructure échange'], result);
        this.checkTitle(archimateView, result);
        if (archimateView.name.toLowerCase().indexOf("applicative") > -1) {

            this.checkFlows(archimateView, result);
        }
        return result
    }




    /**
     * Check that all objects of a given type in a view have mandatory properties
     * and log them if missing
     * @private
     * @param view : the  view
     * @param objectType : the type of concept to check
     * @param mandatoryProps : array of property to check
     */
    checkElements(view, objectType, mandatoryProps, result) {
        let self = this;
        $(view).find().filter(objectType).each(function (app) {
            self.checkMandatoryProps(view, app, mandatoryProps, result)

        });

    }

    /**
     * Check that all objects of a given type in a view has a specialization among 
     * and log them if missing
     * @private
     * @param view : the  view
     * @param objectType : the type of concept to check
     * @param specializations : array of allowed specializations
     */
    checkSpecialization(view, objectType, specializations, result) {
        $(view).find().filter(objectType).each(function (obj) {
            let speFound = false;
            specializations.forEach(element => {
                if (obj.specialization == element) {
                    speFound = true;
                }
            });
            if (!speFound) {
                result.addResult(["WARNING", "'" + obj.specialization + "'" + " specialization should not be used for  " + obj.name, view.name]);


            }
        });

    }

    /**
     * Check that a title block is present and has followinf propertiers
     * note_title, note_date (and correct date YYYY-MM-DD), note_user
     * @param view 
     * @private
     */
    checkTitle(view, result) {
        let titleFound = false;
        $(view).find().filter('diagram-model-note').each(function (note) {
            if (note.prop("note_title") !== null && note.prop("note_title") !== null && note.prop("note_date") != null) {
                if (note.prop("note_date").search("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}") > -1) {
                    titleFound = true
                }

            }
        });
        if (titleFound === false) {
            result.addResult(["WARNING", "no title block or incorrect title block found", view.name]);

        }

    }
    /**
     * Check that flow relationships are labelled as
     * Fxx. label (protocole)
     * check that following properties are set : _protocol, _port,_critical
     * @param view 
     * @private
     * 
     */
    checkFlows(view, result) {
        let self = this;
        $(view).find().filter('flow-relationship').each(function (flow) {
            if (flow.name !== null) {
                if (flow.name.search("^[Ff][0-9]+[a-z]*\.") > -1 && flow.name.search("\([a-zA-Z]+\)$") == -1) {
                    self.checkMandatoryProps(view, flow, ["_protocol", "_port", "_critical"], result)
                } else {

                    result.addResult(["WARNING", "label of flow  '" + flow.name + "' does not comply with rules", view.name]);

                }

            }
        });
    }
    /**
     * @private
     * @param {*} view 
     * @param {*} obj 
     * @param {*} props 
     */
    checkMandatoryProps(view, obj, props, result) {
        props.forEach(element => {
            if (obj.prop(element) == null || obj.prop(element) == "") {

                result.addResult(["WARNING", "mandatory property " + element + " has no value for " + obj.type + " '" + obj.name + "'", view.name]);
            }
        });
    }

    /**
      * get views referenced in the current view
      * @public
      * @param {archimate-diagram-model} archimateView 
      * @returns {ResultSet} with headers "viewName","level" and "message"
      */
    getViewsInView(archimateView) {
        let result = new ResultSet(["Parent view", "Child View"]);
        $(archimateView).find().each(function (child) {
            //when using find("archimate-diagram-model") nothing is returned
            if (child.type !== null && child.type === "archimate-diagram-model") {
                result.addResult([archimateView.name, child.name]);
            }


        })
        return result
    }

}
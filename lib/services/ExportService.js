/**
 * Export Service
 * 
 * 
 */
class ExportService {
    /**
     * Export concept properties and attributes as specified in headers.
     * the header must either match
     * - a property name (exact case need)
     * - an attribute (you can use lower/upper/mixed case : Name, name, NAME all relates to object.name)
     * @param {*} selection 
     * @param {*} headers 
     * @returns 
     */    
    export(selected, headers) {
        let resultSet = new ResultSet(headers)
        
        //
        selected.each(function (o) {
            let data = [];
            
            for (let i = 0; i < headers.length; i++) {
                

                let attrValue = "";
                //check if the header relates to an attribute
                if ( $(o).attr(headers[i].trim().toLowerCase()) !== null ){
                    
                    attrValue = o[headers[i].trim().toLowerCase()];
                } else if (o.prop(headers[i]) !== null) {
                    
                    //try if attrName matches a property name
                    attrValue = o.prop(headers[i]);
                }
                data.push(attrValue);
            }


            resultSet.addResult(data);
        })
        return resultSet
    }
    /**
     * Export concept properties and attributes as specified in headers.
     * automatically add all properties found for each objects.
     * @param {*} selection 
     * @param {*} headers 
     * @returns 
     */
    exportWithAllProperties(selected, headers) {
        let newHeaders = [...headers];
        //iterate over collection to get all properties of selected concepts
        selected.each(function (o) {
            let properties = o.prop();
            for (let i = 0 ; i < properties.length ; i++) {
                let propName=properties[i];
                if (!newHeaders.includes(propName)) {
                    newHeaders.push(propName);
                    
                }
            }
        })
        return this.export(selected,newHeaders)
    }
}